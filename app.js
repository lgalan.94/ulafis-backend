const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const itemRoutes = require('./routers/itemRoutes');
const categoryRoutes = require('./routers/categoryRoutes');
const userRoutes = require('./routers/userRoutes');
const settingRoutes = require('./routers/settingRoutes');

const app = express();
const port = 4002;

mongoose.connect("mongodb+srv://admin:admin@batch288galan.q03dxqw.mongodb.net/LIMS?retryWrites=true&w=majority");
const DBconn = mongoose.connection;
DBconn.on("error", console.error.bind(console, "Error database connection"));
DBconn.once("open", () => console.log('Successfully connnected to cloud database'));

// middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(cors());
app.use('/ulafis', itemRoutes);
app.use('/ulafis/category', categoryRoutes);
app.use('/ulafis/user', userRoutes);
app.use('/ulafis/settings', settingRoutes);


app.listen(port, () => console.log(`Server is running on port ${port}`))