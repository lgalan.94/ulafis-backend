const express = require('express');
const userControllers = require('../controllers/userControllers');
const auth = require('../auth.js');

const router = express.Router();

router.post('/login', userControllers.Login);
router.post('/register', userControllers.RegisterUser);
router.get('/details', auth.verify, userControllers.retrieveUserDetails);

router.get('/details/:userId', auth.verify, userControllers.GetUserDetails);

module.exports = router;