const express = require('express');
const itemControllers = require('../controllers/itemControllers');

const router = express.Router();

router.post('/items/add-item', itemControllers.AddItems);
router.post('/items/add', itemControllers.AddNewItem);
router.get('/items/all',  itemControllers.RetrieveItems);

router.get('/items/:itemId', itemControllers.GetItemById)
router.patch('/items/:itemId', itemControllers.UpdateItem)
router.delete('/items/:itemId', itemControllers.RemoveItem)


module.exports = router;