const settingsControllers = require('../controllers/settingsControllers');
const express = require('express');

let router = express.Router();

router.post('/add', settingsControllers.PostKeyValue);
router.get('/', settingsControllers.getAllSettings);

router.get('/:settingsId', settingsControllers.getSettingsById);
router.patch('/:settingsId', settingsControllers.UpdateSettings);
router.delete('/:settingsId', settingsControllers.getSettingsByIdAndDelete);

module.exports = router