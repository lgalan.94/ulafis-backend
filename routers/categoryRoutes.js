const express = require('express');
const categoryControllers = require('../controllers/categoryControllers');

const router = express.Router();

router.post('/add', categoryControllers.AddCategory);
router.get('/', categoryControllers.RetrieveCategories);

router.delete('/:categoryId', categoryControllers.RemoveCategory);
router.patch('/:categoryId', categoryControllers.UpdateCategory);
router.get('/:categoryId', categoryControllers.GetCategoryById);

module.exports = router;