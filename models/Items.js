const mongoose = require('mongoose');

const itemsSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true
	},
	description: {
		type: String,
		required: true
	},
	quantity: {
		type: Number,
		required: true
	},
	location: {
		type: String,
		required: true
	},
	category: {
		type: String,
		required: true
	},
	low_stock_threshold: {
		type: Number,
	},
	image_url: {
		type:  String,
		required: true
	}
})

const Item = new mongoose.model("Item", itemsSchema);

module.exports = Item;