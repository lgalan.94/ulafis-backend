const mongoose = require('mongoose');

const categoriesSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true
	},
	image_url: {
		type: String,
		required: true
	},
	createdOn: {
		type: Date, default: new Date
	},
	isActive: {
		type: Boolean, default: true
	}
})

const Category = new mongoose.model('Category', categoriesSchema);

module.exports = Category