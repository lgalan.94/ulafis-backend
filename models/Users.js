const mongoose = require('mongoose');

const usersSchema = new mongoose.Schema({
	email: {
		type: String,
		required: true
	},
	password: {
		type: String,
		required: true
	},
	createdOn: {
		type: Date, default: new Date()
	},
	userRole: {
		type: Number, default: 0
	},
	isActive: {
		type: Boolean, default: true
	}
})

const Users = new mongoose.model('User', usersSchema);

module.exports = Users;