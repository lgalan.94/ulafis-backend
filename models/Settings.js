const mongoose = require('mongoose');

const settingsSchema = new mongoose.Schema({
  key: {
    type: String,
    required: true,
    unique: true,
  },
  value: {
    type: String,
    required: true,
  },
  createdOn: {
  	type: Date, default: new Date
  },
  isActive: {
  	type: Boolean, default: true
  }
});

const Settings = mongoose.model('Settings', settingsSchema);

module.exports = Settings;