const Item = require('../models/Items');

module.exports.AddNewItem = (req, res) => {
		const { name, description, quantity, location, category, image_url, low_stock_threshold } = req.body;
		let newItem = new Item({
					name: name.toUpperCase(),
					description: description,
					quantity: quantity,
					location: location,
					category: category,
					image_url: image_url,
					low_stock_threshold: low_stock_threshold
		})
		newItem.save()
		.then(saved => res.send(true))
		.catch(error => res.send(false))
}


module.exports.AddItems = (req, res) => {
  	const items = req.body.map(item => new Item ({
  	  name: item.name.toUpperCase(),
  	  description: item.description,
  	  quantity: item.quantity,
  	  location: item.location,
  	  category: item.category,
  	  image_url: item.image_url,
  	  low_stock_threshold: item.low_stock_threshold
  	}));

  	Item.insertMany(items)
  	  .then(saved => res.send(true))
  	  .catch(err => res.send(false));
}

module.exports.RetrieveItems = (req, res) => {
			Item.find({})
			.then(response => {
						if (response.length > 0) {
								res.send(response)
						} else {
							res.send(false)
						}
			})
			.catch(error => res.send(error))
}

module.exports.GetItemById = (req, res) => {
			Item.findById(req.params.itemId)
			.then(result => res.send(result))
			.catch(error => res.send(error))
}

module.exports.UpdateItem = (req, res) => {
			const { name, description, quantity, location, category, image_url, low_stock_threshold } = req.body;
			let updatedItem = {
					name: name,
					description: description,
					quantity: quantity,
					location: location,
					category: category,
					image_url: image_url,
					low_stock_threshold: low_stock_threshold
			}

			Item.findByIdAndUpdate(req.params.itemId, updatedItem)
			.then(result => res.send(true))
			.catch(error => res.send(false))
}

module.exports.RemoveItem = (req, res) => {
			Item.findByIdAndDelete(req.params.itemId)
			.then(result => res.send(true))
			.catch(error => res.send(false))
}