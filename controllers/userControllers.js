const Users = require('../models/Users.js');
const auth = require('../auth.js');
const bcrypt = require('bcrypt');

/*
User Roles
0 - administrator
1 - Lab Manager
*/

module.exports.RegisterUser = (request, response) => {
	Users.findOne({ email : request.body.email })
	.then(result => {
		if(result === null) {
			let newUser = new Users({
				email : request.body.email,
				password : bcrypt.hashSync(request.body.password, 10)
			})
			console.log(newUser)
			newUser.save()
			.then(saved => response.send(true))
			.catch(error => response.send(false))
			return response.send(result);
		}
	})
	.catch(err => response.send(false))
}

module.exports.Login = (request, response) => {

	Users.findOne({ email : request.body.email })
	.then(result => {
	  if(!result) {
	    return response.send(false)
	  } else {
	    const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

	    if(isPasswordCorrect){  
	      return response.send({auth: auth.createAccessToken(result)})
	    } else {
	      return response.send(false);
	    }
	  }
	})
	.catch(error => {
	  console.log('error:', error);
	  response.send(false);
	});
}

module.exports.GetUserDetails = (request, response) => {		
		const userData = auth.decode(request.headers.authorization);
		if (userData.userRole === 0) {
			Users.findById(request.params.userId)
			.then(result => response.send(result))
			.catch(error => response.send(false))
		} else {
			return response.send(false);
		}
}

module.exports.retrieveUserDetails = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	Users.findOne({_id : userData.id })
	.then(data => response.send(data));
}
