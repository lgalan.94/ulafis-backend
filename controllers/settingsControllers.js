const Settings = require('../models/Settings');
const auth = require('../auth.js');


module.exports.PostKeyValue = (request, response) => {
	
	Settings.findOne({ key : request.body.key })
	.then(result => {
		if(result == null) {
			let newKeyValue = new Settings({
				key : request.body.key,
				value : request.body.value
			})
			newKeyValue.save()
			.then(saved => response.send(true))
			.catch(error => response.send(false))
			
		} else {
			return response.send(false);
		}
	})
	.catch(err => response.send(false))
}


module.exports.getAllSettings = (req, res) => {
	
		Settings.find({})
		.then(result => res.send(result))
		.catch(error => res.send(error))
	
}

module.exports.getSettingsById = (req, res) => {
	Settings.findById(req.params.settingsId)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.UpdateSettings = (req, res) => {
	let UpdatedSettings = { 
		key: req.body.key.toUpperCase(),
		value: req.body.value,
		isActive: req.body.isActive 
	};

	Settings.findByIdAndUpdate( req.params.settingsId, UpdatedSettings)
	.then(result => res.send(true))
	.catch(err => res.send(false))
}


module.exports.getSettingsByIdAndDelete = (req, res) => {
	Settings.findByIdAndDelete(req.params.settingsId)
	.then(result => res.send(true))
	.catch(error => res.send(false))
}
