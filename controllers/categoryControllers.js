const Category = require('../models/Categories');

module.exports.AddCategory = (req, res) => {
	Category.findOne({ name: req.body.name })
	.then(response => {
		if (response === null) {
			let newCategory = new Category({
				name: req.body.name,
				image_url: req.body.image_url
			})
			if (newCategory.name.length < 3) {
				res.send(false)
			} else {
					newCategory.save()
					.then(saved => res.send(true))
					.catch(error => res.send(false))
			}
		} else {
			 res.send(response)
		}
	})
	.catch(error => res.send(false))
}

module.exports.RetrieveCategories = (req, res) => {
		Category.find({})
		.then(response => {
			if (response.length !== 0) {
				res.send(response)
			} else {
				res.send(false)
			}
		})
		.catch(error => res.send(error))
}

module.exports.RemoveCategory = (req, res) => {
		Category.findByIdAndDelete(req.params.categoryId)
		.then(response => res.send(true))
		.catch(error => res.send(false))
}

module.exports.UpdateCategory = (req, res) => {
	let updatedCategory = { 
		name: req.body.name,
		image_url: req.body.image_url
	 };

	Category.findByIdAndUpdate(req.params.categoryId, updatedCategory)
	.then(response => res.send(true))
	.catch(error => res.send(false))
} 

module.exports.GetCategoryById = (req, res) => {
			Category.findById(req.params.categoryId)
			.then(result => res.send(result))
			.catch(error => res.send(error))
}